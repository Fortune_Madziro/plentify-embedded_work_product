/**
 * @file    main.c
 * @author  Fortune Madziro
 * 
 * This program is a Morse code LED flasher.  
 *
 * The functionality of the program is as follows:
 *
 * The program converts a pre-defined string value to its Morse code representation.  
 * The produced Morse code is then displayed through an LED which is connected to 
 * pin 0 of Port A.
 * 
 * Solution Strategy (Algorithm)
 * -----------------------------
 * Store the orse codes for the letters a-z and digits from 0-9 in the one-dimensional
 * arrays called letters and figures respectively.
 * 
 * Create a test string.
 *
 * Convert all the characters that make up the test string to lower case.
 * 
 * Get each character in the edited test string:
 * 
 *     if the character is a letter
 *     then 
 *         Get the morse code for that letter from the letter array.
 *         
 *         Flash the obtained Morse code using the LED.
 *      
 *      else if the character is a digit
 *      then 
 *          Get the morse code for that digit from the figures array.
 *
 *          Flash the obtained Morse code using the LED.
 *
 *      else if the character is a white-space
 *      then
 *          Do nothing for 7 time units.
 */


// PIC32MM0256GPM064 Configuration Bit Settings

// 'C' source line config statements

// FDEVOPT
#pragma config SOSCHP = OFF             // Secondary Oscillator High Power Enable bit (SOSC oprerates in normal power mode.)
#pragma config ALTI2C = OFF             // Alternate I2C1 Pins Location Enable bit (Primary I2C1 pins are used)
#pragma config FUSBIDIO = OFF           // USBID pin control (USBID pin is controlled by the USB module)
#pragma config FVBUSIO = OFF            // VBUS Pin Control (VBUS pin is controlled by the USB module)
#pragma config USERID = 0xFFFF          // User ID bits (Enter Hexadecimal value)

// FICD
#pragma config JTAGEN = ON              // JTAG Enable bit (JTAG is enabled)
#pragma config ICS = PGx1               // ICE/ICD Communication Channel Selection bits (Communicate on PGEC1/PGED1)

// FPOR
#pragma config BOREN = BOR3             // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware; SBOREN bit disabled)
#pragma config RETVR = OFF              // Retention Voltage Regulator Enable bit (Retention regulator is disabled)
#pragma config LPBOREN = ON             // Downside Voltage Protection Enable bit (Low power BOR is enabled, when main BOR is disabled)

// FWDT
#pragma config SWDTPS = PS1048576       // Sleep Mode Watchdog Timer Postscale Selection bits (1:1048576)
#pragma config FWDTWINSZ = PS25_0       // Watchdog Timer Window Size bits (Watchdog timer window size is 25%)
#pragma config WINDIS = OFF             // Windowed Watchdog Timer Disable bit (Watchdog timer is in non-window mode)
#pragma config RWDTPS = PS1048576       // Run Mode Watchdog Timer Postscale Selection bits (1:1048576)
#pragma config RCLKSEL = LPRC           // Run Mode Watchdog Timer Clock Source Selection bits (Clock source is LPRC (same as for sleep mode))
#pragma config FWDTEN = OFF             // Watchdog Timer Enable bit (WDT is disabled)

// FOSCSEL
#pragma config FNOSC = LPRC             // Oscillator Selection bits (Low power RC oscillator (LPRC))
#pragma config PLLSRC = FRC             // System PLL Input Clock Selection bit (FRC oscillator is selected as PLL reference input on device reset)
#pragma config SOSCEN = ON              // Secondary Oscillator Enable bit (Secondary oscillator is enabled)
#pragma config IESO = ON                // Two Speed Startup Enable bit (Two speed startup is enabled)
#pragma config POSCMOD = OFF            // Primary Oscillator Selection bit (Primary oscillator is disabled)
#pragma config OSCIOFNC = OFF           // System Clock on CLKO Pin Enable bit (OSCO pin operates as a normal I/O)
#pragma config SOSCSEL = OFF            // Secondary Oscillator External Clock Enable bit (SOSC pins configured for Crystal mode)
#pragma config FCKSM = CSECME           // Clock Switching and Fail-Safe Clock Monitor Enable bits (Clock switching is enabled; Fail-safe clock monitor is enabled)

// FSEC
#pragma config CP = OFF                 // Code Protection Enable bit (Code protection is disabled)

#include <xc.h>
#include <ctype.h>
#include <string.h>
#include <time.h> 

#define _XTAL_FREQ 20000000
#define LED_PIN PORTAbits.RA0
#define LOW 0
#define HIGH 1
#define DOT_DELAY 500

/*definition of function prototypes*/
void to_lower_case(char str[]);
void flash_LED(char morse_code[]);
void delay(int delayValue);

/*letters contains the morse codes for the letters a-z respectively*/ 
char *letters[] = {
	".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..",
	".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.",
	"...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."
};

/*figures contains the morse codes for the figures 0-9 respectively*/
char *figures[] = {
	"-----", ".----", "..---", "...--", "....-",
	".....", "-....", "--...", "---..", "----."
};

const int dot_delay = DOT_DELAY;
const int dash_delay = DOT_DELAY * 3;
const int space_delay = DOT_DELAY * 7;

int main(void)
{
    int i;
    int index;
    
    char character; 
    
    char test_string[] = "This is a test! Testing 1, 2, 3!";
    
    /*set pin 0 of Port A as an output pin*/
    TRISAbits.TRISA0 = 0;
    
    LED_PIN = LOW;
    
    /*convert to lower case*/
    to_lower_case(test_string);
    
    for(i = 0; i <= strlen(test_string); i++)
    {
        character = test_string[i];
        
        /*check if character is a letter or a digit*/
        if(isalnum(character))
        {
            /*if character is a letter*/
            if(isalpha(character))
            {
                /*get morse code for the letter*/ 
                index = character - 'a';
                
                flash_LED(letters[index]);
                
            }
            /*if character is a digit*/
            else
            {
                /*get morse code for the digit*/
                index = character - '0';
                
                flash_LED(figures[index]);
            }
        }
        /*if the character is white-space*/
        else if(isspace(character))
        {
            /*wait for 7 time units*/
            delay(space_delay);
        }
    }
    
    return 0;
}

void to_lower_case(char str[])
{
    int i; 
    
    for(i = 0; i <= strlen(str); i++)
    {
        tolower(str[i]);
    }
}

void flash_LED(char morse_code[])
{
    int i;
    
    for(i = 0; i <= strlen(morse_code); i++)
    {
        /*if morse code character is a dot*/
        if(morse_code[i] == '.')
        {
            /*switch on the LED*/
            LED_PIN = HIGH;
            
            /*wait for 1 time unit before switching off LED*/
            delay(dot_delay);
            LED_PIN = LOW;
            
            /*wait for one time unit after displaying a dot or dash*/ 
            delay(dot_delay); 
        }
        /*if morse code character is a dash*/
        else
        {
            LED_PIN = HIGH;
            
            /*wait for 3 time units before switching off LED*/
            delay(dash_delay);
            LED_PIN = LOW;
            
            /*wait for one time unit after displaying a dot or dash*/ 
            delay(dot_delay);
        }
    }
}

void delay(int delayValue) 
{ 
    /*store the start time*/ 
    clock_t start_time = clock(); 
  
    /*wait until the required delay is generated*/
    while (clock() < (start_time + delayValue)); 
}